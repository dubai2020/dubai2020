var gulp = require('gulp'), 
    gutil = require('gulp-util'),
    browserSync = require('browser-sync').create(),
    autoprefix = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    merge = require('merge-stream'),
    minifyCss = require('gulp-minify-css');


// Static Server + watching scss/html files
gulp.task('serve', ['css'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("assets/scss/**/*.*", ['css']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('css', function() {

	return gulp.src("assets/scss/*.scss")
        .pipe(sass({
            "outputStyle": "expanded",
            "sourceComments": "true",
        }).on('error', gutil.log))
        .pipe(autoprefix({
            browsers: ['last 3 versions'],
            cascade: false
        }))
/*        .pipe(minifyCss({compatibility: 'ie8'})) */
        .pipe(gulp.dest("css"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
